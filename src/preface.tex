\chapter*{Preface}

In the beginning, there was \GURPS.

Well, no. In the beginning there was \textit{Dungeons \& Dragons}, and after
that, its many imitators.  After those, there was \textit{The Fantasy Trip},
and \textbf{then} there was \GURPS.  But, as far as we're concerned, \GURPS\ is
where we start.

\GURPS\ is a roleplaying game system by Steve Jackson Games (God bless them,
every one) designed to facilitate as wide a variety of settings and genres as
possible.  \textit{Designed} as such, not merely \textit{intended}---it is, in
my opinion, a masterwork of game design accomplishing everything it sets out to
do.  At the time of the release of the game's first edition in 1986, the notion
of a set of roleplaying rules for multiple settings and genres was a novelty.
\GURPS\ carved a cosy niche for itself, and there it sat.

But time passed (as it's been known to do), and the world of gaming shifted.
Many of \GURPS' design decisions fell out of fashion, and while other games,
like \sout{TSR's} Wizards of the Coast's \textit{Dungeons \& Dragons} and
\sout{White Wolf's} Onyx Path's \textit{World of Darkness} adapted, Steve
Jackson's \GURPS\ has remained relatively unchanged across its four
editions---changed enough to warrant multiple editions, but not too much to
still be considered the same system\footnote[1]{Contrast this with \DnD, which
has fundamentally reinvented itself as a game with every major edition by
Wizards of the Coast.}.

As the world of gaming changed, other systems began to intrude upon \GURPS'
niche as a generic system for many genres and settings.  As early as 1989, the
superhero-themed game of \textit{Champions}, from which the \GURPS\ system
itself takes much inspiration, had its system generalized and released as a
setting- and genre-agnostic roleplaying toolkit called the \textit{HERO} 
system, almost certainly in response to \GURPS' success.  However, the greatest
blow to \GURPS' status came in the year 2000\ in the form of the d20 System,
which Wizards of the Coast developed for its new edition of \DnD\ after
acquiring TSR, Inc.  For this system, Wizards of the Coast developed the Open
Gaming License, a permissive copyright license for game rules, modeled after
open-source software licenses such as the GNU GPL and the MIT License.  The
license granted independent publishing companies near total freedom to use
certain content in any set of rules licensed thereunder.  By releasing the d20
system under the Open Game License; Wizards of the Coast, not entirely on
purpose, enabled anyone to write, publish, and sell content for and supplements
using the system.

The d20 System took the world by storm.  Its quirks, intricacies, and design
principles cemented themselves in the collective consciousness of the gaming
community.  The system was adapted for a broad variety of settings and genres
by many different publishers, taking advantage of people's familiarity with its
idioms and conventions from \DnD.  Wizards of the Coast themselves released a
present-day-fantasy-themed game called \textit{d20 Modern}---which worked about
as well as you would expect the transplantation of the \DnD\ rules into a
modern setting to work.  Even Chaosium's \textit{Call of Cthulhu} got an
official d20 conversion, and that was even worse.  For almost a decade, the d20
system had a near monopoly on roleplaying game design, and to this writing
still has a monopoly on the campaigns of many gaming groups.  Gamers and
publishers alike used the system's perceived flexibility to adapt the system
for countless settings.  d20 quickly became the square peg for every round hole
in tabletop gaming.

In the midst of this, the \GURPS\ system began to seem outdated and outmoded. 
\GURPS\ was not like d20.  Its mechanics seemed confusing and counterintuitive
to a public accustomed to the omnipresence of \DnD-isms.  It seemed unnecessary
to learn a new system rather than converting and reflavoring an already-familiar
one.  Although the d20 system's stranglehold on the market has long gone slack,
to this writing, the perception of \GURPS\ as strange, difficult, and
superfluous system remains.

Exacerbating this problem are a number of things that make the \GURPS\ system
generally less accessible than d20.  The closest thing \GURPS\ has to a core
rulebook, \GURPS\textit{ Basic Set}, has far more information than is needed
for any single campaign of \GURPS; while the decision to include all this
information in the core makes sense based on the design assumptions made by
the system, the reader cannot be expected to know those assumptions.  The
organization of \textit{Basic Set} has no excuse; it works better as an 
encyclopedia than as a quick reference or an introductory rulebook.  Most
significant, however, is the fact \GURPS\ has failed to adopt what is perhaps
the greatest innovation of modern gaming: the Systems Resource Document.

In writing \GGG, I intend to at once rememdy all three problems with \GURPS:
\GGG\ is to provide an accessible, well-organized, comprehensive, and
freely-available rules reference for \GURPS-style games.  I also intend to
annotate the text heavily to offer insights into the design assumptions of the
game, clear up misconceptions, offer advice, and suggest house rules.  My hope
is that this work will ease the adoption of \GURPS\ into the system library of
many roleplaying groups.  \GURPS\ is a wonderful system, and it is a tragedy
that, through no fault of its own, it goes untried and unconsidered for many
groups that, if given guidance, would enjoy it very much.

Thank you for taking the time to read these rules, and if you enjoy them, then
I urge you to consider supporting Steve Jackson Games by purchasing \GURPS\
material.  This text covers only a tiny fraction of the content available for 
\GURPS, most of it from \textit{Basic Set}, with bits from \textit{Powers},
\textit{Magic}, and \textit{Martial Arts}.  While I may or may not write
further material for \GGG\ in the future, in that material I intend to take
more liberties from the source material than in this text.  I am not sure how
influential this text will be, but my hopes are high.  Again, thank you very
much for reading \GGG!

Valedictions, \linebreak
\textit{Vivit Elric}


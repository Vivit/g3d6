\chapter*{Introduction}

Welcome.

We'll spare you the usual spiel of ``WHAT IS A ROLEPLAYING GAME?'' that comes
at the beginning of almost every roleplaying rulebook ever.  This text is
intended foremostly for \kw{game masters} (``\kw{GMs}'')  considering
running a game with \GGGd, who already have at least some experience in other
roleplaying games, either as a GM or as a player.  That said, we will do our
best to keep this text as intelligible as possible to the novice.

If you don't know what a \kw{roleplaying game} is, you can, in this age of
the Internet, very easily find out, but from the words ``roleplaying'' and
``game'' alone, it should be easy enough to guess their meaning: it's a game,
and you roleplay.  The term that even experienced gamers do often seem to fail
 to understand is ``\kw{generic roleplaying system}'', or, as we shall
abbreviate it in this text, the \kw{GRPS}.  Thus, instead of describing what an
 RPG is, we will take this time to describe the GRPS, and more importantly,
\GGGd's particular design approach to the GRPS.

\GGGd\ is written with the following four key points of design in mind:

\partitle{Absolute generality}
A GRPS is, as we use the term, \textit{a roleplaying system that supports and
facilitates a wide variety of genres and settings with minimal alterations to
the body of its core mechanics}.  \GGG\ is intended to, in theory, support any
setting imaginable!  This forms the first key point of \GGG's design:
\kw{absolute generality}.  The core mechanics should be as broadly applicable
as possible!

\partitle{Modularity}
\GGG\ is a highly \kw{modular} game: its rules and mechanics are designed to
function well independently from one another, so that they may be safely
altered or replaced without jeopardizing the balance or the cohesion of the
rest of the rules.  Many of the rules may be removed altogether if desired.
Furthermore, this text is written with the express goal of separating the
fundamentals from the advanced details, making it easy to sort out which rules
you want and which ones you don't want.  This is the second key point of
\GGG's design.

\partitle{Internal compatibility}
\GGG\ in particular is designed with the intent to be sufficiently cohesive
and consistent to allow characters from entirely different campaigns, settings,
and genres to be transplanted into the same campaign under the same set of
rules.  It is not merely the unplayable skeleton of a system to be converted
for a variety of settings!  This \kw{internal compatibility} is the third
point of \GGG's design.

\partitle{Plausible verisimilitude}
\GGG\ intends to provide rules to, as believably as possible, simulate the
settings and genres it supports.  This does not mean that it intends to be an
accurate simulation of reality on every level of the system---in the case of
unrealistic settings, a believable simulation of the setting will make the game
\textit{less} realistic!  Thus, the goal is not realism \textit{per se}, but
\kw{plausible verisimilitude}---the fourth key point of \GGG's design.

Also warranting mention are a number of much simpler design assumptions which
are nonetheless fundamentally misunderstood by many gamers attempting to play
other generic roleplaying systems, leading to no end of myths, memes, and
misconceptions.  The reader would do well to remember the following meta-rules
of \GGG:

% Maybe tone is a little too harsh?
\partitlenopunc{Doctor, Doctor, it hurts when I do \textit{this}!}
\GGGd\ has many rules.  \textit{You are not expected to use all of them at
once}.   The rules are not \textit{meant} to all be used at once.  If, when you
fail to recognize this, you bite off more than you can chew and end up having
Bad Funtimes, know that the fault lies \textit{\textbf{entirely with you}} for
failing to heed this warning!  Please do not complain to me (Vivit Elric), or
to online message boards, or to anyone else other than yourself for the
consequences of your own foolishness.

If ever you find yourself frequently interrupting play to look up the details
of the rules you are using, or find yourself or your players distracted from
play by the cognitive load of the rules you are using, please, for your own
sake, \textit{consider using a different combination of rules.}

A rule does not have to be explicitly labeled as optional for you to dispense
with it!  If any rule seems too complicated, feel free to try the game without
it.  It's good to come back and experiment with those rules later, but you
should never force yourself to use rules you're not comfortable with.

\partitle{Whatever it is, it depends on the setting}
Absolute generality may mean that \GGGd\ can support any setting, but that does
not mean that everything will have the same effect on every setting.  Taking a
9 millimeter submachine gun back in time into the iron age would obviously give
it a very different meaning. 

Not \textit{even}, but \textit{especially} game balance depends on the setting.
Using combat rules or permitting advantages and skills written for cinematic
swashbuckling or high-flying \textit{wuxia} would both break the balance and
ruin the tone of any campaign more down-to-earth.  Even something as simple as
a particular distribution of skill points can work fine in one setting, but
completely break entire adventures in another.

\partitle{The GM's word is first and final}
As GM, it is your power and your responsibility to decide what will and will
not be appropriate to your campaign.  While you should be careful not to forbid
clever or creatively unorthodox ideas, do not feel obligated to allow something
just because it is in this text.  Do not \textit{follow} the rules; \textit{use}
them---and acknowledge that the final word on what is permitted and what is not
is yours alone.  This is easily the single most important meta-rule, to which
all the others can be reduced.

We understand well that inexperienced GMs might not be able to tell what sort
of builds will prove problematic and what sort of builds will not, and in what
settings and campaigns---this is a major issue to be addressed in the 
annotations, giving GMs more confidence to forbid builds that have the
potential to break campaigns.

Remember this, if nothing else: \nokitchensink.
\goodbreak

Got it?  Good.  Now let's begin.

\section{Basics}
\GGG\ has two central mechanics: \kw{3d6 under} and \kw{point buy}.  If you are
familiar with other roleplaying games, you likely have at least a passing
familiarity either with these concepts or with similar ones.  \textit{D\&D BX}
rolls 3d6 under for attribute checks.  \textit{AD\&D}, \textit{D\&D BX}, and
games using the \textit{Basic Roleplaying} GRPS feature a \textit{1d100 under}
mechanic, sometimes called ``d\%''.  All recent editions of \textit{D\&D} have
options for buying ability scores with points at character generation, and
\textit{D\&D 3rd Edition} and \textit{v. 3.5} award \textit{skill points},
which players may spend to improve their skills, on level up.  However, the
ways in which \GGGd\ uses the mechanics of 3d6 under and point buy are distinct
in certain respects, particularly in their \textit{orthogonality} and their
\textit{ubiquity} throughout the entire \GGGd\ system.  \textit{All} success
rolls are 3d6 under, and \textit{all} advancement is managed with point buy
from a single point pool.

We will now give a brief overview of these two mechanics.  The presentation
style of the following information is to be taken as an example of the style
that we will use throughout the rest of this text.  Note that when terms are
defined, we will typically define some synonyms for those terms as well.  This
is not because all of those synonyms will be \textit{used} in this book, but
rather to define the terms as they are used among roleplayers.  We will do our
best to use only one or two forms of each term throughout this text.

\subsection{3d6 Under}     \label{3d6under}
\kw{3d6 under} is \GGG's mechanic for handling \kw{success rolls} (also called
\kw{tests} or \kw{checks}), where three ordinary, cubical six-sided dice are
rolled, and the sum of the values facing up on the dice compared to some
\kw{target number} (or \kw{TN}).  If this sum is \textit{less than or equal to}
the target number, the roll is a \kw{success}; otherwise, it is a \kw{failure}.
This target number is often based on a statistic on the player's character
sheet.

A roll whose target number is equal to the character's \DX\ score is variously
called a \kw{\DX\ roll}, a \kw{\DX\ test}, a \kw{\DX\ check}, or a \kw{roll vs.
\DX}.  To \kw{attempt} a \DX\ roll means to roll the dice and compare their sum
to \DX; to \kw{make} a \DX\ roll means to attempt a \DX\ roll and get a success.
An action is \kw{tested against} or \kw{checked against} \DX\ if it requires a
successful \DX\ roll to be executed properly.

\subsubsection{Boni and Mali} \label{adj}
Depending on the circumstances, an additive adjustment may be applied to the
target number of a test, resulting in the roll being easier or harder to make.
Adjustments that raise the target number, making the roll easier, are called
\kw{boni} (singular \kw{bonus}).  Conversely, adjustments that lower the target
number, making the roll harder, are called \kw{mali} (singular \kw{malus}) or
\kw{penalties}.

Note that all arithmetic relating to boni and mali is to be applied to the
target number, never to the roll itself.  Making a success roll at a +1 bonus
means rolling against a target number 1 point higher than the situation would
otherwise indicate.  For success rolls, no modifier is ever applied to the
result on the dice.

\subsubsection{Criticals} \label{crits}
The specific rolls of 18, 17, 3, 4, and sometimes 5 and 6 may be considered 
special cases called \kw{criticals}, or colloquially, ``\kw{crits}''.  Rolls of
3 and 4 are \kw{critical successes} and rolls of 17 and 18 are \kw{critical
failures}.

Criticals typically have special effects varying depending on context.  For
example, when an attack roll is being attempted, a \textit{critical hit} cannot
be dodged, and has an additional effect depending on the result of a roll on
the \textit{critical hit table} (pg. \pageref{crithittable})---or, if simpler
combat rules are desired, just does double basic damage.

For any test against a $\mathit{TN}\geq15$, rolls of 5 will also be counted as
critical successes. Similarly, for any test against a $\mathit{TN}\geq16$,
rolls of 6 will also be counted as critical successes.  Furthermore, for any
test against a $\mathit{TN}\geq15$, rolls of 17 are not counted as critical
failures, only as ordinary failures.  For any test where $\mathit{TN}\leq6$,
any roll greater than or equal to $\mathit{TN}+10$ is a critical failure.  All
this should be evaluated \textit{after} the application of boni and mali.

\begin{sidebar}{Simplifying the Rules: Static Critical Margins}
    Critical margins that vary with target number are a perfect example of an
    implicitly optional rule.  If you would rather have only 3's and 4's be
    critical successes and have all 17's and 18's be critical failures, go
    ahead and play that way!  It won't break anything.
\end{sidebar}

\subsection{Point Buy}
\kw{Point buy} is a mechanic for controlled character building and progression
whereunder stats and abilities are bought with \kw{points} (\reff{ch1cp}).

\subsubsection{Advantages and Buying Up}
Points are spent to make the character more powerful.  Attributes (described in
\textbf{chapter \ref{chap:attrib}---Attributes, pg. \pageref{chap:attrib}}) and
skills (described in \textbf{chapter \ref{chap:skill}---Skills, pg.
\pageref{chap:skill}}) may be \kw{bought up} to quantitatively improve a
character's abilities, and special traits called \kw{advantages} may be bought
to afford qualitative benefits and \textit{special} abilities not necessarily
available simply through training the body and mind.

\subsubsection{Disadvantages and Buying Down}
Points may be granted for making the character less powerful.  Attributes (but
not skills!) may be bought \kw{bought down} to limit the of the in some ways
while freeing up points to be spent elsewhere, making the character more
powerful in other ways.  Traits called \kw{disadvantages} may be bought to
afford points to the player for bearing the burden of special challenges not
faced by most people.

Note that any exchange of points for stats or traits is called \textit{buying},
regardless of whether points are spent in exchange for benefits or granted for
taking detriments!

\subsubsection{Free vs. Restricted Point buy}
Under the basic rules, all points come from the same resource pool:  points
spent raising the basic attributes of are points that could otherwise be spent
on skills, advantages, and any other mechanical benefit in the system, and
points gained from taking disadvantages and lowering attributes may likewise be
spent on any attribute, advantage, or other benefit the GM allows.  This is
called \kw{free point buy}.

There are, however, optional rules to allow GMs to restrict point expenditure,
either for balance purposes or to alleviate the decision paralysis new players
will experience upon being presented with a such a wide array of possiblity as
afforded by free point buy.  These means of abstraction are collectively called
\kw{restricted point buy}, and primarily function by offering traits together
in groups as \kw{meta-traits}, or by laying out a \kw{template} within which
to build a character.  Restricted point buy is detailed in \textbf{chapter}
\reff[Meta-Traits and Templates]{chap:temp}.

\rule{\textwidth}{1pt}

These two mechanics are the core of \GGGd.  With them in place, the GM is free
to change anything and everything about the rest of the system.  As long as the
specific implementations of 3d6 under and point buy described in this text are
used, the game being played may still be called \GGGd.  \textit{All} of the
rest is up to you.  If you don't like something, change it!  If you have an
idea, try it!  These rules are designed to be tinkered with.

Experiment.  Solicit feedback.  Explore.  Learn.  Your adventurous spirit
should extend beyond the adventure of the characters in the game world; it
should be the driving force behind all you do at the table, not even, but
especially the way you play and run the game itself.

There is only one wrong way to play this game, and that is whatever way does
not facilitate the enjoyment of the game by the group.  As a GRPS, this is a
system meant to be used in many different ways.  Do not limit yourself to only
one of these ways, let alone a way you don't like.  This advice should go
without saying, but it seems to be far too often forgotten:

Whatever you decide to do, \textit{make sure you have fun doing it.}

% \section{}

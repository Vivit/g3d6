\chapter{Overview of the \textit{G3} Character} 
\GGGd, like most roleplaying games, mechanically manages any character, either
played by a player as a \kw{player character} (``\kw{PC}'') or by the GM as
a \kw{non-player character} (``\kw{NPC}''), as an aggregate of information
usually written down on a \kw{character sheet}.

\GGG\ is a classless, level-less system.  No rule forbids any character from
enjoying any ability so long as sufficient \kw{character points} are available
to be spent.  No categories such as occupation or experience level provide or
prohibit access to any character building option, though there are optional
rules that allow such mechanics to easily be emulated if desired.

To aid understanding, this chapter will give a brief overview of the mechanical
data on the \GGG\ character.  Not all of it is mandatory; much of this
information relates only to optional rules that may be easily ignored for the
sake of simplicty.  \Nokitchensink\textbf{.}

\input{statlist.tex}

\section{Types of Information}  \label{ch1stat}
This information may be categorized into two types: numerical
(``\kw{statistics}'' or ``\kw{stats}'') and categorical
(``\kw{properties}'').  Also important, but not shown above is descriptive
information (``\kw{details}'').

Statistics including \kw{attributes}, \kw{skills}, and miscellaneous other
data, and are measured in \kw{points}.  A character with \ST\ 10 is said to
have ten \textit{points of ST}.  This is different from having ten
\textit{points in ST}---see \textbf{\ref{ch1cp}--Character Points and Point
Buy} below.

Because the word ``statistic'' may be used in other senses in some contexts,
the term \kw{character statistics} will be used to refer specifically to those
statistics recorded on the \GGG\ character sheet.

Properties most importantly include \kw{traits}; which are divided into
\kw{advantages}, \kw{disadvantages}, and \kw{features}.  Traits can be
anything from ambidexterity to psychiatric disorders to superpowers.

\kw{Details} include nonmechanical information like name, physical
appearance, date of birth, etc., which are not listed above; but also other
details may be more directly important, like the terms of oaths or codes of
honor.  These mechanically-relevant details are typically appended to certain
advantages and disadvantages, and will be described in full in the descriptions
of those traits.  Any information on the character with negligible mechanical
effect, especially flavor details, we will call ``\kw{embellishments}''.

\section{Character Points and Point Buy}    \label{ch1cp}
A special, very important statistic exists called \kw{character points},
usually abbreviated as ``\CP'' or simply shortened to ``points''.  Points
are spent to raise statistics and confer beneficial properties, and may be
refunded by lowering statistics and accepting detrimental properties.  The
mechanic of exchanging points for benefits is called \kw{point buy}.

Recall that in \GGGd, the word ``buy'' is used both for spending points on
benefits and being granted points for detriments!  Spending points to increase
a statistic is called ``\kw{buying up}''; lowering a statistic in exchange
for additional points is called ``\kw{buying down}''.  Any exchange of points
for traits is simply called ``buying''.  Synonymous with the latter sense of
``to buy'' is ``to take'', especially in the context of disadvantages, but for
the sake of consistency, only the word ``buy'' will be used in this text.

Recall also that the word ``point'' is also used as a general term for the unit
of any statistic (\reff{ch1stat}).  A character with \ST\ 10 [0] is said to
have 10 \textit{points of}\ \ST, even though zero \textit{character} points
have been spent on the attribute.


\subsection{Point Cost}     \label{ch1pcost}
The \kw{point cost} of a stat or trait  is denoted with a number inside
[square brackets].  Positive numbers denote points that must be spent, while
negative numbers denote points granted.  For convenience, a trait's cost will
often be written after its name; for example, \tr{Danger Sense}{15}.

For brevity, instead of always spelling out  ``$n$ point(s)", this text will
wherever possible abbreviate quantities of points using the bracket notation.
``[1]'' is to be read as ``one point''.

\subsection{Point Total}    \label{ch1ptotal}
The total points available for a character to spend on statistics and traits
is called the character's \kw{point total}.  Any points left over from
character creation, or earned through play and not yet spent, are simply
called \kw{unspent points}, and are recorded on the character sheet for later
use.  No buy ever changes the character's total; a proportional adjustment is
made to the character's unspent points instead.

A character's point total, sans unspent points, may be used as a rough
approximation of the character's general competency, but do note that it is
impossible to ensure that this relation is perfectly proportional.  \GGG's
character generation is complex and detailed, and with this inevitably comes
the fact that some characters' point distributions will yield a greater ratio
of points in to power out than the point distributions of other characters.

This unavoidable degree of imbalance is, however, well-mitigated by the nature
of \GGGd\ as a generic roleplaying system: it would not make sense to allow
\textit{Magical Aptitude 3} in a realistic, history-conscious campaign about
mid-20th-century military operations.  Like everything else, point cost balance
\DotS.  The reader would do well to remember that \nokitchensink.  GMs, beware
of minmaxers, and minmaxers beware!

\section{Attributes}    \label{ch1at}
The simplest type of stat on the character sheet is the \kw{attribute}, which
represents general physical and mental capabilities.  An attribute score may be
raised by one point of that attribute by spending its point cost in \CP, or
lowered by one point of that attribute in exchange for a number of \CP\ equal
to its point cost.  Attributes are often used as base target numbers for
success rolls.

\subsection{Basic Attributes}   \label{ch1ba}
A character has exactly four \kw{basic attributes}: \ST, \DX, \IQ, and \HT.
The base value of these attributes is \textit{always 10}, and the only variable
affecting a character's basic attributes is the number of points spent on it,
positive or negative.

\subsection{Secondary Attributes}
All attributes other than the basic attributes are called \kw{secondary
attributes}, and each is based either on one or more basic attributes.  Instead
of adding or subtracting from 10, secondary attributes are adjusted relative to
the attributes on which they are based.  Their point cost is significantly
lower than those of the basic attributes.

\begin{sidebar}{Simplifying the Rules: Basic Attributes Only}
    If you want to keep the attribute rules simple, you are free to ignore
    secondary attributes altogether during character generation!  Spending
    points on secondary attributes is for fine-tuning characters, and is very
    much an optional rule.

    In many roleplaying games, some stats are used to represent odd
    combinations of faculties---like wisdom being used both for literal wisdom
    and for the ability to notice hidden things, causing the venerable elderly,
    who have accumulated much wisdom from many years' experience, to have
    sharper senses than spry young folk in the prime of their youth!

    \GGGd\ avoids this problem by allowing different faculties to be adjusted
    seperately.  While the strong of mind might also often be strong of will,
    it's entirely possible and indeed likely for a dim-witted but warm-hearted
    and valiant warrior to be of far stronger of moral fiber than a brilliant
    but sheltered and cowardly scholar.

    However, the \GGG\ character creation process is a complicated beast, so
    if, as a GM for new or less mechanically-inclined players, you have not
    unwisely decided to trim the fat, you may very easily dispense with the
    mechanic of custom secondary attributes.  This makes character generation
    significantly less complicated, and should be among the first
    simpilificatory measures you consider.  Remember that \nokitchensink.
\end{sidebar}

\section{Irregular Secondary Characteristics}
Irregular secondary characteristics are statistics not bounded in the range
suitable for testing against as attributes.  Some of them record specific,
concrete figures regarding characters' abilties---how much weight they can
lift, how fast they can run, etc.

\section{Skills}    \label{ch1skill}
In contrast to general physical and mental faculties stand \kw{skills}, which
represent specific, acquired proficiencies.  Each skill is linked to a single
attribute, usually \DX\ or \IQ.  Unlike with secondary attributes, however,
this attribute score is not used as the default value of a skill with no points
spent on it!  To have a score in a skill at all, at least [1] must be put into
that skill (although some skills may be used by characters without any score in
them---see \textbf{\ref{sdefault}--Skill Defaults} on page \pageref{sdefault}
for details).

\section{Traits}    \label{ch1trait}
\kw{Traits} are divided into \kw{advantages}, which are beneficial and have
a positive point cost; \kw{disadvantages}, which are detrimental and have a
negative point cost; and \kw{features}, which are neutral and have no point
cost---these may, but do not necessarily, qualify as embellishments.

Some traits may be bought in multiple \kw{levels}, providing further effects
at multiples of their point costs.  Of some traits, both positive and negative
levels may be bought; such traits thus qualify both as advantages and as
disadvantages.

\subsection{Perks and Quirks}
\label{ch1pequirk}
\kw{Perks} and \kw{quirks} are very minor advantages and disadvantages with
point costs of $[1]$ and $[-1]$, respectively.  Perks exist as minor niceties for
players to enjoy at their option, while quirks primarily exist to give more
flavor to a character.  The negative effects of quirks are usually little more
than very minor roleplaying restrictions.

\subsection{Trait Modifiers}    \label{ch1traitmod}
If desired, \kw{modifiers} may be applied to traits to enhance them or limit
them and alter their cost.  Instead of point costs, modifiers have percentile
point cost adjustments, expressed as a positive or negative percent in [square
brackets].  The modifiers themselves are listed along with their cost
adjustments after the trait in \{curly braces\}, as in \textit{Flight}
\{\trmod{No Hover}{-15}, \trmod{Newtonian Spaceflight}{+25}\} [44].

\section{Miscellanea}
Finally,  there are the properties of \kw{wealth level}, \kw{status level}, and
\kw{languages known}, which do not fit easily into any of the above categories.
They are not traits, because all characters have them; and they are not
secondary attributes or characteristics, because they are not connected to the
four basic attributes. 

\rule{\textwidth}{1pt}

When supervising character creation, it is important to keep in mind the
meanings of the character building decisions players make.  In creating their
characters, they are defining the context and building the foundation for their
roleplay.  Many character building options impose roleplaying restrictions on
the player.  In taking those options, players agree to abide by their
restrictions.  If they persistently fail to do so, they are acting out of
character!

Of course, as with everything, these restrictions are technically optional.  If
they prove too restrictive to the character concepts that players have in mind,
you are free to waive or modify them.  These restrictions are not simply for
balance; many of them are for verisimilitude.  Encourage players to voice their
concerns.  Open and honest communication is crucial to a satisfying campaign.
Simply refusing to follow the rules of the game is not an appropriate response
for players that find your rules excessively restrictive, and is one of the
most common types of bad roleplaying.

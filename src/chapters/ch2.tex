\chapter{General Statistics}    \label{chap:attrib}

The following chapter deals with \kw{attributes} and \kw{secondary
characteristics}.  These statistics form the core of a \GGG\ character, so you
should read this chapter carefully.  The most important section by far is
\reff{ba}{Basic Attributes}.  With these attributes only you can run a game.
The rest of the chapter deals with rules for further fine-tuning a character's
core statistics and for determining other important, but less central
\textit{secondary characteristics} like hit points (\reff{hp}) and rate of
movement (\reff{bm}).

For a very simple game, use basic attribute rules the rules in section \ref{ba}
and refer to section \ref{hp} to determine hit point totals, and to section
\ref{bs} to determine basic speed and basic move.

\rule{\textwidth}{1pt}

\section{Attributes}
\kw{Attributes} are statistics that define for a character those abilities
which are not directly acquired through education or training.  They are
broadly-applicable and influence many other statistics.  They are the simplest
type of character statistic in \GGGd.  All attributes may be used as a target
number for \kw{attribute tests} (see \ref{attest} below). Attributes are
described in detail in this chapter, and appendix A (pg. \pageref{appa})
provides a table of all the attributes, their costs, and their base values.

Statistics based on attributes are called \kw{secondary characteristics}.  Some
of these, called \kw{secondary attributes}, are attributes themselves.

\subsection{Attribute Tests}   \label{attest}
Just as attributes are the simplest type of statistic, the simplest type of
success roll is the \kw{attribute test}: a test (pg.  \pageref{3d6under}) with
a target number equal to some attribute score, plus or minus a bonus or malus.
For example, catching a thrown ball might be handled with a \DX\ test.

\subsection{Basic Attributes}   \label{ba}
The basic attributes are \ST, \DX, \IQ, and \HT.  All of these have a base
score of 10---if no points of a specific basic attribute are bought for a
character, the character's score for that attribute will be 10.  The ordinary
range of basic attributes for a functioning human is 8 to 12, but higher or
lower values are certainly possible.  The following benchmarks should be
helpful, although this scale will almost certainly be inflated in groups that
prefer heroic, larger-than-life characters:

\begin{itemize}
    \item A BA score of 6 is severly debilitating, likely requiring
        assisted living.
    \item A BA score of 7 is the lower limit for competent functioning in
        society at large.
    \item A BA score of 8 or 9 is below average, but not to such a degree as
        to significantly impede everday tasks.
    \item A BA score of 10 is perfectly average.
    \item A BA score of 11 or 12 is above average, but certainly not
        exceptional.
    \item A BA score of 13 or 14 is exceptional, outclassing most people.
    \item A BA score of 15 or 16 is prodigious, showing great potential indeed.
    \item A BA score of 17 is the peak of ordinary human achievement.  Anything
        higher could rightly be called superhuman.
\end{itemize}

The correlation between a basic attribute score and the number of points spent
on the attribute is absolute.  No trait or statistic influences any basic
attribute other than the number of character points spent on that attribute.
While certain traits may alter the effective score of a basic attribute for
specific purposes, such as \textit{Limb ST} (pg. \pageref{adlimbst}), these
traits are best considered analogous to secondary attributes.

The attributes of \DX\ and \IQ\ are called the \textit{technical basic
attributes}---those basic attributes which, with few exceptions, govern all
\textit{skills} in the game.  Skills requiring physical finesse are handled
by \DX, and skills requiring social awareness or cognitive ability are handled
by \IQ.  Because of their importance to all kinds of specialized competency,
\DX\ and \IQ\ cost twice as much as \ST\ and \HT.

\subsubsection{ST [10]}
\label{st}
\kw{\ST}, or \textit{strength} is the statistic representing a character's
capacity for the exertion of physical force through natural bodily means.  High
\ST\ normally implies high muscle mass, good muscle tone, or both.  The
secondary characteristics based on \ST\ are \BL\ (\reff{bl}) and \HP\ 
(\reff{hp}).  It also affects the combat statistics of swing and
thrust damage (\reff{dmgroll}).

Any feat of strength may be tested against \ST, though if specific figures for
the forces involved are known, \BL\ may be considered first.

\subsubsection{DX [20]}
\label{dx}
\kw{\DX}, or \textit{dexterity} is the statistic representing a character's
quickness, agility, kinesthetic sense, and general coordination.  Its
definition is thus broader than the usual sense of the English word
``dexterity''.  The only secondary characteristic based on \DX\ is \BS\
(\reff{bs}), but this in turn affects \BM\ and a character's dodge score
in combat.

Tests requiring quick reflexes will often be handled as \DX\ tests if no more
specific statistic is available.  Attack rolls with all weapons are based on
\DX, as well as most other skills not directly related to intellectual or
cognitive functioning.

\subsubsection{IQ [20]}
\label{iq}
\kw{\IQ}, or \textit{intelligence} is the statistic representing a character's
mental faculties.  It is not necessarily connected to the score a character
would receive on a real intelligence quotient assessment, but the typical range
for basic attributes is such that it would usually not be unrealistic to equate
one point of \IQ\ score with ten points of intelligence quotient.  The
secondary attributes based on \IQ\ are \Per\ and \Will.

Attempts to recall \textit{general} in-character knowledge not necessarily known
to the player out-of-character are often tested against \IQ.  Specialized
knowledge, however, may require a related skill test instead.

\subsubsection{HT [10]}
\label{ht}
\kw{\HT}\ is the statistic representing overall health, wellness, endurance,
and stamina.  The secondary characteristics based on \HT\ are \Vigor, \Vital,
\FP, and \BS, which in turn affects \BM\ and dodge score in combat.

\subsection{Secondary Attributes}  \label{sa}
\kw{Secondary attributes} are more specific than basic attributes.  They allow
characters to be more finely tuned than they would be if their abilities were
characterized only by broadly-defined categories like strength and intellect.
A sharply perceptive eye might be associated with a strong mind, but the
archetypical absent-minded professor provides a very immediate example of a
strong mind with very little awareness of its surroundings.  Thus, the secondary
attribute of \Per\ is, with no points put into it, equal to \IQ, but may be
bought up and down like any other attribute.  To express the connection between
the \IQ\ statistic and the \Per\ statistic, we say that \Per\ is \kw{based on}
or \kw{uses} \IQ.

Secondary attribute scores are capped at 20.  If buying up other attributes
would raise a simple secondary attribute score above 20, it is not unreasonable
to consider the secondary score to be automatically bought down to 20 and to
refund \CP\ accordingly, even if buying down attributes after character
generation is otherwise forbidden.


\subsubsection{Vigor [2] (HT)}          \label{vigor}
\kw{\Vigor} represents a character's ability to remain conscious despite
physical trauma or physiological shock.  Tests against the \Vigor\ attribute
are rolled when a character's \HP\ is less than or equal to zero, but may also
be called for after exposure to incapacitating agents such as tranquilizers or
sleeping gas.

\subsubsection{Vital [2] (HT)}          \label{vital}
\kw{\Vital}, or \textit{vitality} is similar to \Vigor, but is instead used
for rolls to survive potentially lethal injuries and conditions.  \Vital\ tests
may be rolled to, for example, survive a life-threatening dose of poison, but
are most commonly rolled when a character's \HP\ (see below) falls below a
negative multiple of its maximum.

For realistic campaigns, consider capping both \Vigor\ and \Vital\ at $\HT+2$.

\subsubsection{Will [5] (IQ)}           \label{will}
\kw{\Will} represents a character's ability to resist psychological stress,
including coercion by threat, torture, seduction, and more exotic forms of
compulsion such as brainwashing and mind control, whether supernatural or
mundane.  Other supernatural assaults may also be tested against \Will\ even if
they do not involve coercion or manipulation.

\Will\ may also be used for attempts to control or manipulate one's own
thoughts through conscious, disciplined mental effort.  Skills relating to
meditation or and autohypnosis, as well as many types of psi powers, are
based on \Will.


\subsubsection{Per [5] (IQ)}            \label{per}
\kw{\Per}, or \textit{perception} represents a character's general
attentiveness, perceptiveness, and to some extent, the ability to analyze
information.  \Per\ tests may be rolled to detect hidden things, either by
finding them from a focused search or noticing them when attention is
directed elsewhere.  Because \Per\ tests relate to the gathering of
information, they are typically rolled by the GM in secret, especially for
rolls to notice things when not actively searching.  \Per\ tests and other
information-gathering tests are described in further detail in \reff{qery}.

\begin{sidebar}{For the Mechanically Minded}
    Given that the cost of \IQ\ is [20] and the cost of \Per\ is [5], the
    formula for the  net cost of \IQ\ and \Per\ is $[20(IQ-10)+5(Per-IQ)]$.

    To build our absent-minded professor, we first buy up \IQ\ well above
    average, buying \Per\ down by one point for every point of \IQ, keeping our
    \Per\ score at 10.  The net cost of these buys is thus $20b-5b$, or $15b$,
    for some natural number $b$ representing how \textit{brilliant} our
    professor is relative to the average mind.

    Next, we buy \Per\ further down to a score well below average, making the
    net cost $15b-5a$ for some natural number $a$ representing how
    \textit{absent-minded} our professor is relative to the average functional
    human being.  We may then adjust $a$ and $b$ to taste.
\end{sidebar}

\section{Other Secondary Characteristics}
The following secondary characteristics are not considered attributes because
they cannot be rolled against as target numbers.  Their range may be wider than
those of attributes, and not all of them have a fixed cost per point---damage
score (pg. \pageref{}) is not even measured in points at all!

\subsection{Depletable Secondary Characteristics}
\kw{Depletable secondary characteristics} are statistics that may be temporarily
gained and lost over the course of an adventure.  Depletables actually have
two different scores: the \kw{current score} and the \kw{maximum score}.  The
current score may not be raised above the maximum score by natural means,  
although the attribute may be bought up with \CP, of course.  For each point of
a depletable attribute bought, both the current score and the maximum score of
that attribute are raised by 1 point.

We will denote the maximum of depletable attribute $AT$ as \textatmax\AT.

\subsubsection{HP [2] (ST)} \label{hp}
\kw{\HP}, or \textit{hit points} are a depletable characteristic providing an
abstraction for physical injury.  The lower an \HP\ score relative to
\textatmax\HP, the worse injured the character is.

Any loss of \HP\ is called \kw{damage}.  Damage is normally determined by
the result of a \textit{damage roll}, which follows different rules from the
standard 3d6 under mechanic used for tests.  For more information, refer to
\reff[Damage Rolls]{dmgroll}.

If \HP\ falls to zero or below, a successful \Vigor\ test will be required to
remain conscious each turn.  Moreover, whenever the character's \HP\ score
falls below a negative multiple of \textatmax\HP, a \Vital\ roll to survive the
ordeal must be made immediately, or the character dies.

For more information on character death, see \reff[Death and Dying]{death}.
For more information on the effects of damage, see \reff[Damage]{damage}.

For realistic campaigns, it is advisable to require players to keep their
\textatmax\HP within $\pm30\%$ of their \ST\ scores---this means that if a
character has \ST\ 10, \textatmax\HP may only be directly bought up to 13.
If further \textatmax\HP are desired, \ST\ must first be bought up.
Similarly, if \textatmax\HP fewer than 7 are desired, \ST\ must first be
bought down.  Whether to round up or round down for purpose of this
percentage is your decision as GM.

The ranges of other secondary attributes may be similarly restricted if
desired.  Do note, however, that for campaigns of swashbuckling cinematics
or other over-the-top action where realism is a secondary concern, you will
probably have more fun if you loosen these restrictions or remove them
entirely.

\subsubsection{FP [3] (HT)}
\kw{\FP}, or \textit{fatigue points} are a depletable characteristic providing
an abstraction of mental and physical fatigue.  The lower an \FP\ score relative
to \textatmax\FP, the more exhausted the character is.

Any loss of \FP\ is called \kw{fatigue}.  Fatigue may be determined by damage
rolls from certain attacks, but it is more often lost as a result of heavy
exertion of the body.  Many magic systems use \FP\ as a power source.

If \FP\ falls to zero or below, the character becomes totally exhausted.
\Will\ tests will be required to focus on even simple tasks.  Further exertion,
bringing \FP\ negative, is possible, but for every point of fatigue taken when
$FP \leq 0$, 1 point of \HP\ is also deducted.

For more information on \FP\ and its uses, see \reff[Fatigue and Exhaustion]{fatigue}.

\begin{sidebar}{Customizing the Rules: Why are \HP\ based on \ST\ and not \HT?}
    It's customary in roleplaying games to have seperate statistics for
    strength and constitution, and to derive hit points from the latter
    statistic.  \GGG\ handles \HP\ differently because it conceives \HP\ 
    differently: in games such as \DnD, hit points are not a simple metric of
    physical injury (``meat points''); they are an abstract, mechanical
    representation of the ability to keep fighting---a combination of injury
    tolerance, defensive ability, endurance, and even luck or divine favor.

    Hit points are not like this in \GGGd.  Here, hit points \textit{really
    are} ``meat points''.  They represent physical injury and nothing else.
    Anatomically, muscles provide a sort of armor for the vitals, especially
    if they are well-developed.  \HP\ is based on \ST\ because \GGGd\ is
    intended to be able to support realistic campaigns where the protective
    function of the muscular system, which is very relevant to combat in real
    life, is observed.

    However, there are advantages to basing \HP\ on \HT\ instead.  \ST\ has
    a directly offensive use, since melee damage rolls are based on \ST\
    (\reff{dmgroll}).  With both \HP\ and melee damage based on \ST, \ST\ 
    governs both how much damage a character can deal and how much the
    character can take.  If you wish to mechanically separate offense and
    defense, it is not at all a bad house rule to base \HP\ on \HT\ and
    \FP\ on \ST.
\end{sidebar}

\subsection{Irregular Secondary Characteristics}         \label{irreg}
\kw{Irregular secondary characteristics} are characteristics with a more
complicated formula for determining their score than a simple base value and a
flat cost per point.  Do not worry, they are not that complicated, and once you
have calculated them and written them on your character sheet, you won't have
to calculate them again.  \textit{Swing} and \textit{Thrust} damage are not
calculated at all---refer to the table below to determine them.

\BL\ and \BM\ also have units of measure---meters per second and kilograms,
respectively.  For ease of calculation, the metric system will be used for all
units of measure in \GGGd.  (Be glad you don't have to deal with irrational
numbers and ugly universal constants like the the speed of light and the
gravitational constant, because when those are in the equation, the metric
system can't save you.)  In the cost brackets, the solidus should be read as
``per''.  For example, \BM\ costs [5] character points \textit{per} meter per
second.

\subsubsection{Damage [5 / \textit{special}] (ST)}
\begin{wraptable}{r}{0.4\textwidth}
\vspace{-20pt}
\begin{tabular}{| c | c | c |}
\hline
Where\ldots      & \sw\ is\ldots & \thr\ is\ldots  \\ \hline
$1 \leq \ST < 3$ & \dice{1}{-6} & \dice{1}{-5}\\ \hline
$3 \leq \ST < 5$ & \dice{1}{-5} & \dice{1}{-4}\\ \hline
$5 \leq \ST < 7$ & \dice{1}{-4} & \dice{1}{-3}\\ \hline
$7 \leq \ST < 9$ & \dice{1}{-3} & \dice{1}{-2}\\ \hline
$\ST = 9$        & \dice{1}{-2} & \dice{1}{-1}\\ \hline
$\ST = 10$       & \dice{1}{-2} & \dice{1}{}  \\ \hline
$\ST = 11$       & \dice{1}{-1} & \dice{1}{+1}\\ \hline
$\ST = 12$       & \dice{1}{-1} & \dice{1}{+2}\\ \hline
$\ST = 13$       & \dice{1}{}   & \dice{2}{-1}\\ \hline
$\ST = 14$       & \dice{1}{}   & \dice{2}{}  \\ \hline
$\ST = 15$       & \dice{1}{+1} & \dice{2}{+1}\\ \hline
$\ST = 16$       & \dice{1}{+1} & \dice{2}{+2}\\ \hline
$\ST = 17$       & \dice{1}{+2} & \dice{3}{-1}\\ \hline
$\ST = 18$       & \dice{1}{+2} & \dice{3}{}  \\ \hline
$\ST = 19$       & \dice{2}{-1} & \dice{3}{+1}\\ \hline
$\ST = 20$       & \dice{2}{-1} & \dice{3}{+2}\\ \hline
\end{tabular}
\vspace{-50pt}
\end{wraptable}
This statistic actually encompasses two different scores: \textit{swing} damage,
abbreviated \sw, and \textit{thrust} damage, abbreviated \thr.  For every
[5] spent on this statistic, determine \sw\ and \thr\ as if \ST\ were one
point higher.  Refer to the table on the right.

For an extended version of this table with \ST\ scores going up to 100, see
page \pageref{dmgtablefull}.

\subsubsection{BL [3 / \textit{special} kg] (ST)}   \label{bl}
\BL, or \textit{basic lift} is the statistic representing a character's
ability to lift and carry heavy objects.  As a benchmark, it represents the
maximum weight that a character can lift overhead with one hand in one
second under normal conditions. Its base value in kilograms is equal to half
the square of \ST\ (that is, $\frac{ST^2}{2}$).  For details on the main
ways \BL\ is used, see \reff[Lifting]{lifting} and
\reff[Encumbrance]{encumb}.

For every [3] spent on \BL, calculate \BL\ as if \ST\ were one point higher.
For example, a character with \ST\ 10 and [6] spent on \BL\ would have a
\BL\ score in kilograms equal to $\frac{(10+2)^2}{2}$, or 72kg. 

\subsubsection{BS [5] (DX, HT)}               \label{bs}
\BS, or \textit{basic speed} is the statistic representing quickness of
movement, action, and reaction.  Its base value is equal to the sum of
\DX\ and \HT (that is, $\DX+\HT$). In battle, combatants take turns in
descending order \BS.  Furthermore, the statistics \BM\ and \Dodge\ are
both based on \BS.

\BS\ may be bought directly for [5].  \BM\ is based on \BS\ divided by four,
rounded down, so it may be efficient to buy \BS\ up to the next multiple of
four.

\subsubsection{BM [5 / 1 m/s] (BS)}                 \label{bm}
\BM, or \textit{basic move} is the statistic representing \textit{locomotor}
speed.  It is measured in meters per second, and by default is equal to one
quarter of \BS, rounded down.  It is \textit{running} speed, not walking speed.
With the base values of \DX\ and \HT\ both being 10, the average human has a
\BS\ of $20$ and a \BM\ of $5$ meters per second.  That would be a very brisk
walk!

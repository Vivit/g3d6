\chapter{Traits}            \label{ch:traits}
\kw{Traits} are special qualities that distinguish an individual from the
average human, beneficial (\textbf{\ref{ad}--Advantages}) or detrimental
(\textbf{\ref{disad}--Disadvantages}).  Traits vary widely in how they are
bought and how they manifest in play.  In this chapter we will describe common
types of traits and the rules associated with them.  A list of all the traits,
with detailed descriptions of each, may be found in appendix C (pg. 
\pageref{appc}).

\section{Trait Types}
Other than being simply ``good'' or ``bad'', traits may be categorized in terms
of their function.  \kw{Trait type} is effectively, a mechanical categorization
for the rules of individual traits---for example, advantages that take effect 
on cue are of the \textit{active ability} trait type (\textbf{\ref{actabil}}).

\subsection{Roll Adjustment}    \label{rolladjtrait}
A \kw{roll adjustment trait} is a trait with the effect of a bonus to specific
rolls.  \textit{Brachiation} (pg. \pageref{a:brachiat}), for example, gives a
$+2$ bonus to all \textit{Climbing} (pg. \pageref{s:climbing}) skill rolls,
entirely independently from the skill itself.  Roll adjustments are generally
not the only effect of a trait---\textit{Brachiation} also allows the bearer to
\textit{brachiate}, or swing from tree-branch to tree-branch at
\textonehalf\BM---but exceptions are possible.

\subsection{Social Traits}
\kw{Social traits} are traits that relate to a character's relationships,
social obligations, and social capital.  Many of them may express themselves
as reaction adjustments (\reff{react}), while others indicate the existence of
some important relationship with some non-player character---rivalry, emnity,
debt, social connections, or the like.

\subsection{Non-Constant Traits}
Not all traits are in effect at every moment. 


\subsection{Scalable Traits}   \label{traitlevel}
Some traits come in levels, allowing them to be bought up and down not unlike
attributes.  The cost of a scalable trait is expressed in the form $[cn]$ for
some integer constant $c$ representing the cost of the trait per level and an
arbitrary integer $n$ representing how many levels of the trait have been
bought.  For example, if a trait costs [10] per level, its cost would be
expressed in the form $[10n]$.

\begin{sidebar}{Customizing the Rules: Scalable Trait or Secondary Attribute?}
In many cases, the categorization of a character building option as either a
scalable trait or a secondary attribute is largely arbitrary.  The most common
difference is that while secondary attributes may always be bought both up and
down, scalable traits may only be bought up if an advantageous version of the
trait is avaialable, and may only be bought down if a disadvantageous version
of the trait is available.

%% TODO link up
Take, for example, the advantage \textit{Magical Aptitude} (pg.
\pageref{a:magery}).  All spells in the basic magic system (\textbf{chapter}
\reff{ch:magic}) are learned as skills based on the sum of \IQ\ and the number
of levels of \textit{Magical Aptitude} that have been bought.  Thus, the
\textit{Magical Aptitude} trait is meant to represent the ``magic gene'' that,
in many settings, is required for all magic use.  It would usually not make
sense to allow players to buy negative levels of \textit{Magical Aptitude}.
For this reason, and because it would not make sense to complicate the
attribute rules with a mechanic that is only relevant to certain settings, we
have chosen to make \textit{Magical Aptitude} into an advantage rather than a
secondary attribute.

However, there certainly may be a place for a \Mag\ attribute!  Perhaps you
have a setting where everyone can cast at least some magic.  In this case, it
would make sense for there to be people who are intelligent, but generally inept
at magic.  This is not at all unlike the absent-minded professor with high \IQ\ 
but low \Per, so in this context it would make sense to treat \textit{Magical
Aptitude} as a secondary attribute rather than as a trait.

You may also want a \Mag\ attribute simply for the sake of convenience.  Any
campaign that features heavy magic use could benefit greatly from the addition
of a field on the character sheet for the sum of \IQ\ and \textit{Magical
Aptitude}.  In this case, buydown below \Mag\ should generally \textit{not} be
allowed: ``Magical Ineptitude'' simply means you can use no magic!
\end{sidebar}

\section{Trait Modifiers}                   \label{trmod}
\kw{Trait modifiers} are special options that change how traits work. Modifiers
are selected when a trait is bought, and a percentile adjustment to the point
cost of the trait is applied.  Modifiers that augment the effects of a trait
are called \kw{enhancements}, and modifiers that diminish the effect of a trait
are called \kw{limitations}.  Note that in the case of disadvantages, an
enhancement worsens the effects of a disadvantage, while a limitation partially
mitigates it.

Modifiers may be applied when the trait is initially bought, or may be added
and removed afterward as a part of character progression.  In particular,
limitations on advantages may be bought off to improve the advantage, and
limitations may be added to disadvantages to mitigate their effects.  All
changes to existing traits are subject to GM approval---in general, there
should be a justification for all changes made to existing traits.

The modifiers applied to a trait are listed after the skill in \{curly braces\},
each followed by its point cost adjustment, as in \textit{Flight} \{\trmod{No
Hover}{-15}, \trmod{Newtonian Spaceflight}{+25}\} [44].  To calculate the final
cost of the trait, take the sum of the adjustments and add or subtract the 
appropriate fraction of the trait's cost, discarding any remainders. In this
example, \textit{Flight} is an advantage worth [40], \textit{No Hover} is a
limitation worth $[-15\%]$, and \textit{Newtonian Spaceflight} is an enhancement
worth $[+25\%]$. $-15\%+25\%=10\%$, and $10\%$ of 40 is 4, so the final cost of
the trait is [44].

Modifiers may not decrease the final cost of a trait below $20\%$ of its base
cost.

\section{Advantages}                        \label{ad}
Traits beneficial to the bearer are called \kw{advantages}.  These may represent
superpowers, natural talents, social connections, or other helpful qualities 
beyond simply being especially strong, fast, or skilled.  Some are in effect
constantly, while others must be manually activated.  Others simply grant boni
to specific types of rolls.

\subsection{Active Abilities}               \label{actabil}
Some advantages, like \textit{Special Attack} (pg. \pageref{ad:spcatk}) have no
effect until the character chooses to use them.  These advantages are called
\kw{active abilities}.  How long thes abilities take to activate and under what
conditions they may be activated varies depending on the advantage and on what
limitations are applied to the advantage.

Note that just because an ability is voluntary does not mean it is necessarily
under the user's full control.  The limitation \textit{Uncontrolled} (pg.
\pageref{m:uncont}) causes the ability to sometimes activate by accident, and
the limitation \textit{Involuntary} (pg. \pageref{m:uncons}) causes the user to
have no control over their ability at all!  Similarly, many limitations exist
that limit the conditions under which the ability may be activated.

\section{Disadvantages} \label{disad}
Traits detrimental to the bearer are called \kw{disadvantages}.  These may
represent physical disabilities, psychological disorders, supernatural curses,
emnities with powerful individuals, social stigmata, obligations, and even oaths
or codes of honor.  Anything that limits a character's freedom of action may be
considered a disadvantage.

\subsection{Low Attributes vs. a Disadvantage}  \label{disability}
Low attributes are generally not a good way to represent, for example, specific
medical disabilities.  Specific disabilities are typically more limited in scope
than even secondary attributes while also having more specific effects than can
be represented solely with attributes.  Disadvantages may thus be used to
encapsulate the special rules required to more thoroughly model a disability.

Consider the example of a peg leg prosthesis.  Realistically, a peg leg hampers
not only mobility (\reff[Basic Move]{bm}), but also kicking ability, and indeed
any action involving the use of the legs!  For this reason, we have the
\textit{Lameness} (\pageref{da:lame}) disadvantage, specifically the
\textit{Missing Leg} variant.  While the trait does require that \BM\ be bought
down to 2, a realistic peg leg is an impediment to far more than just movement,
and is thus itself worth 30 points in addition to the points granted in exchange
for the buydown of \BM.

\subsection{Behavioral Disadvantages}    \label{psychdisad}
\kw{Behavioral disadvantages}, such as \textit{Greed} and \textit{Bad Temper},
require players to adjust their roleplay within certain restrictions.  Failure
to do so is considered poor roleplay---taking the disadvantage constitutes an
agreement to roleplay in a certain way in exchange for points.  This is grounds
to withhold character points!

\subsubsection{Self-Control}
Such disadvantages present an interesting dilemma.  A player character with a
behavioral disadvantage may be prone to making choices that the player would
rather not make!  Players generally prefer to feel that they are in control of
their characters.  How, then, can mechanical representations of the natural
inclinations of character be reconciled with the natural inclinations of the
player?  As a compromise, we have the mechanic of the \kw{self-control roll},
which represents a character's ability to resist a specific temptation.  When
presented with a situation under which a behavioral disadvantage may manifest,
the player may choose to attempt a self-control roll.  On a successful roll,
the player may ignore the requirements of the disadvantage in that specific
situation.

\begin{wraptable}{r}{0.3\textwidth}
\vspace{-20pt}
\begin{tabular}{| l | l |}
\hline
\multicolumn{2}{|c|}{\SC\ Cost Adjustments} \\
\hline
\SC\ 6  & $+100\%$  \\ \hline
\SC\ 9  & $+50\%$   \\ \hline
\SC\ 12 & $+0\%$    \\ \hline
\SC\ 15 & $-50\%$   \\ \hline
\end{tabular}
\vspace{-20pt}
\end{wraptable}
The base target number of a self-control roll is 12.  \Will\ has no effect, as
a behavioral disadvantage is a personality trait that cannot be changed through
willpower alone.  The self control number may be bought up as a limitation to
the disadvantage down as an enhancement to the disadvantage.

Players generally should not attempt a self-control roll at every opportunity.
Self-control rolls are primarily for situations where the behavior could cause
serious problems for the character or the party.  There is no hard limit on how
often self-control rolls may be attempted---the issue is not the frequency of
the attempts, but the motivation.  If you feel that a player is using the
self-control mechanic as an excuse to act out of character, feel free to impose
a penalty for poor roleplay.  A dynamic way to discourage the abuse of \SC\ 
rolls is a progressively increasing malus to the target number of the roll.

Similarly, it is \textit{good} roleplaying for players to color their behavior
with the disadvantage even when in situations that would not force a specific
course of action.  Even when not slighted, a character with \textit{Bad Temper}
might act in a generally surly manner.  How to interpret the disadvantage in
play is up to the player---\textit{Bad Temper} might express itself as angry
outbursts, or as snide, half-veiled insults, or as a passive-aggressive "silent
treatment".  Reward creativity, do not punish it.

\subsubsection{Buyoff}
Buyoff of a behavioral disadvantage may indicate a significant change in the
personality of a character.  You may wish to allow it only in response to
life-changing experiences, whether sudden and immediate or extended over a
longer period.  The exception to this is in the case of codes of honor and
other self-imposed behavioral disadvantages---it is still a major change in
the character, but not one that requires anything more than a personal
decision to change.

\subsection{Social Disadvantages}           \label{socdisad}
\kw{Social disadvantages} are disadvantages in the form of 

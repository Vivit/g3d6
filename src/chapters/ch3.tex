\chapter{Skills} \label{chap:skill}
\kw{Skills} are statistics representing proficiencies that can be practiced and
learned.  The way they operate is not unlike simple secondary attributes, but
the rules for determining their cost are different.  They are also different
from attributes in their specificity---a character's \DX\ score influences the
target numbers for all melee attacks, while the \sk{swordplay (broadsword)}{}
skill is only tested against when making an attack with a balanced, one-handed,
single- or double-edged 60-to-120 centimeter blade.

To describe the relationship between skills and attribute, we say that a skill
\kw{is based on} or \kw{uses} the attribute.  For example, we say that the
\sk{broadsword}{} skill \textit{uses} the \DX\ attribute. 

In this chapter we will detail the skill rules, but will \textit{not} provide
a list of skills---for that, refer to appendix B on page \pageref{appb}.

\section{Skill Difficulty and Buying Skills}
\label{skilldiff}
\label{buyskill}
Skills do not have point costs the same way that attribute scores do.  Rather,
they have \kw{difficulties}, which determine the base value of the skill when a
single point is spent on it.  Putting [1] into an \kw{easy} \IQ\ skill would
yield a skill level equal to the character's \IQ\ score.  The level of an
\kw{average} skill with [1] spent on it would be $IQ-1$; of a \kw{hard} skill,
$IQ-2$; and of a \kw{very hard} skill, $IQ-3$.  This malus is called the
\kw{difficulty malus}.

Regardless of difficulty, the first level of a skill costs [1], the second
costs another [1] (bringing the total cost to [2]), the third costs another
[2] (bringing the total cost to [4]), and each further level costs a further
[4].  Unlike attributes, skills may not in any event be bought down below [0]
to grant additional points.

\begin{tabular}{| c | c | c | c | c |}
\hline
    Cost for    & \multicolumn{4}{|c|}{Given difficulty\ldots}  \\ \cline{2-5}
    skill level\ldots
                & Easy        & Avg     & Hard        & V.Hard      \\  \hline
     $AT-3$     & $-       $  & $-   $  & $-       $  & $[1]     $  \\  \hline
     $AT-2$     & $-       $  & $-   $  & $[1]     $  & $[2]     $  \\  \hline
     $AT-1$     & $-       $  & $[1] $  & $[2]     $  & $[4]     $  \\  \hline
     $AT  $     & $[1]     $  & $[2] $  & $[4]     $  & $[8]     $  \\  \hline
     $AT+1$     & $[2]     $  & $[4] $  & $[8]     $  & $[12]    $  \\  \hline
     $AT+2$     & $[4]     $  & $[8] $  & $[12]    $  & $[16]    $  \\  \hline
     $AT+3$     & $[8]     $  & $[12]$  & $[16]    $  & $[20]    $  \\  \hline
     $AT+4$     & $[12]    $  & $[16]$  & $[20]    $  & $[24]    $  \\  \hline
     $AT+n$     & $[4(n-1)]$  & $[4n]$  & $[4(n+1)]$  & $[4(n+2)]$  \\  \hline
\end{tabular}

\section{Skill Notation} \label{snotation}
A skill is denoted on the skill list in the form \sk{skill}{\AT\Diff}, where
\textit{Skill} is the name of the skill, \AT\ is the attribute used by the
by the skill, and \Diff\ is the difficulty of the skill.  For
\sk{broadsword}{}, we write \sk{broadsword}{\DX\Avg}.  For specialized skills
(see \textbf{\ref{sspecial}--Specialization} below), the attribute and
difficulty rating are parenthesized separately from and placed \textit{after}
the name of the specialty, as in \sk{science (biology)}{\IQ\Hard}.

On the character sheet, skills are denoted the same way, but with the figures
of \textit{points spent} and \textit{relative score} appended:
\sk{broadsword}{\DX\Avg} [1] $-1$.  The points spent are in [square brackets]
as usual, and the $-1$ indicates that the skill score is equal to $DX-1$.  For
ease of play, you may also want to write the absolute score at the end of this
line, as in \sk{broadsword}{\DX\Avg} [1] $-1$ $=9$, but realize that if you do this
for every skill, you may have a lot of updating to do if you ever buy up \DX\ 
or \IQ!

\section{Skill Prerequisites}   \label{sprereq}
\kw{Skill prerequisites} are conditional restrictions placed on the advancement
of a skill.  Some skills require attributes to be within a certain range, while
others require one or more \textit{traits} (\textbf{chapter} \reff{ch:traits}).
Although the prerequisites for most skills are simple or nonexistent, some
skills have more prerequisites than others.  Such skills are rare and powerful,
and require special mastery to even begin to learn them.  Of course, rarity is
relative, so all prerequisites may \textit{\textbf{depend on the setting}}.

If a skill has more than one prerequisite, these prerequisites may
\textit{conjoined} with an ``and'' qualifier or \textit{disjoined} with an
``or'' qualifier.  A set of conjoined prerequisites itself forms a
prerequisite.  Parentheses will be used to disambiguate where necessary.  For
example, to express that the skill \sk{flying leap}{} requires the skills of
\sk{jumping}{} and \sk{power blow}{} in addition to one of either of the
advantages \textit{Hidden Arts} and \textit{Weapon Master}, we write that
\sk{flying leap}{} has the prerequisites of ``(skills \sk{jumping}{} or
\sk{power blow}{}) and advantages \textit{Hidden Arts} and \textit{Weapon
Master}''.

Prerequisites provide a way to express the relative inaccessibility of such
skills in ways beyond skill difficulty.  If prerequisite trees sound too
complicated, the alternative is \kw{prerequisite by fiat}, where the GM
dictates characters' eligibility to learn advanced skills based on context and
common-sense judgments.  Recall that \nokitchensink.

\section{Skill Defaults} \label{sdefault}
The \kw{default} score of a skill on which no points are spent varies from
skill to skill; sometimes it is a different skill score at a malus, sometimes
it is the skill's base attribute at a malus, and sometimes it is nothing at
all.  If the default of skill $A$ is equal to the score of a skill $B$ at a
\kw{default malus} of $-2$, we say that the default of $A$ is $B-2$, or that
$A$ \kw{defaults to} $B$ \kw{at} $-2$.

Note that, although more difficult skills tend to have less favorable default
mali than easier skills, default malus is independent from difficulty malus!
\sk{Shortsword}{} and \sk{rapier}{} are both \DX\Avg\ skills dealing with
sword-fighting, but are far more different from one another than
\sk{shortsword}{} is from \sk{broadsword}{}, so \sk{broadsword}{} and
\sk{rapier}{} default to one another at $-4$ rather than $-2$.

\subsection{Advanced Default Rules}
    If you feel comfortable that you understand well how skill defaults work,
    or if you believe you have come across a strange edge case of the rules for
    skill defaults, read on.  Otherwise, you may skip on to
    \reff[Specialization]{sspecial} below.

    \subsubsection{Improving Skills from Default}
    Experimentation with different distributions of points among skills with
    defaults will reveal that, given the above rules alone, it is possible to
    end up wasting points by buying skills to a level that is already given to
    the character by default.

    Consider \sk{shortsword}{} and \sk{broadsword}{}, both \DX\Avg\ skills that
    default to one another at $-2$.  A character with \sk{broadsword} {\DX\Avg}
    [4] $+1$ enjoys a default of \sk{shortsword}{\DX\Avg} [0] $-1$.  Now suppose
    that the player chooses to spend [1] on \sk{shortsword}{}.  According to
    \ref{buyskill}, this would yield a score of $DX-1$, which the character
    already receives for free as a default!

    To remedy this, you may want to allow \textit{improvement from default},
    where the cost of each skill known at default is reduced by the number of
    skill points required to buy the skill to the level afforded by the default.
    In our \sk{shortsword}{} example, improvement from default would allow the
    player to buy \sk{shortsword}{} up to $+0$ for [1], because the [1] required
    to bring the character's \sk{shortsword}{} score to $-1$ is deducted from the
    cost.  Buying \sk{shortsword}{} up to $+1$ from there would cost a further
    [2] as usual, so it would cost [3] total to buy \sk{shortsword}{} up to $+1$
    from a default of $-1$.

    One way to denote improvement from default on the character sheet is to
    write the cost of the skill's default score in the brackets after the skill
    name, appending an asterisk* to indicate that those points are not actually
    counted in the character's point total:

    \begin{itemize}
        \item \sk{broadsword}{\DX\Avg} $[4]$ $+1$
        \item \sk{shortsword}{\DX\Avg} $[1^*]$ $-1$
    \end{itemize}

    When improving from default, simply add the points you are actually
    spending to the figure in the brackets with a plus sign:
    \begin{itemize}
        \item \sk{broadsword}{\DX\Avg} $[4]$ $+1$
        \item \sk{shortsword}{\DX\Avg} $[1^*+3]$ $+1$
    \end{itemize}

    \subsubsection{Multiple and Circular Defaults}
    If a character knows one skill at default, and some other skill would
    default to \textit{that} skill, then for the latter default to take effect,
    at least one point must be spent on the former skill, even if this would
    not improve that skill from default: \sk{rapier}{} defaults to
    \sk{broadsword$-4$}{} and \sk{broadsword}{} defaults to
    \sk{shortsword$-4$}{}, but \sk{rapier}{} does not default to
    \sk{shortsword$-8$}{}.  However, as long as at least [1] is spent on
    \sk{broadsword}{}, you may use your \sk{broadsword}{} score as a default
    for \sk{rapier}{}, even if that [1] does not improve your \sk{broadsword}{}
    score above \sk{shortsword$-4$}{}.

    Note that ``circular defaults'' are never allowed: if a character knows
    \sk{shortsword}{} at default from \sk{broadsword}{}, the character's
    \sk{broadsword}{} score itself receives no benefits from the
    \sk{shortsword}{} score.

    \subsubsection{Reallocation of Points Spent On Skills}
    Because skill defaults can get complicated so quickly, GMs especially of
    new players are advised to allow character points spent on skills to be
    freely reallocated, and which skills are being used at default from which
    other skills to be reselected.  It is, generally speaking, not very
    conducive to enjoyment for players to be held to character building
    decisions made during previous sessions, especially because plans can
    change quickly as characters develop through roleplay.

    You may be tempted to restrict this numerical fiddling as stressing over
    mechanics to the exclusion of roleplay, but you should note that making all
    character building decisions final may lead players to stress \textit{more}
    intently over character building decisions for fear of making errors that
    can't be taken back!  A reasonable compromise may be the restriction that
    the reallocation of skill points should not lower any skill scores without
    justification (such as the character getting ``rusty'' at a skill that goes
    unpracticed for an extended period).

\section{Specialization}    \label{sspecial}
Some skills actually represent a category of closely related skills connected
by a common thread.  Such variant skills are called \kw{specialties}.  Some
skills \textit{must} be specialized, as they are too broad to be considered a
single field, while for other skills, specialties are optional foci of some
broader discipline.  Specialties are expressed in (parentheses) after the skill
name.

No specialty may be used in place of other specialties of the same skill unless
it explicitly defaults to those specialties--this is often, but not always,
the case. Each specialty should be considered a skill unto itself, related to
but not interchangeable with other specialties of its super-skill.
Specialization is less a game mechanic and more a nomenclatural convention for
categorizing distinct but conceptually related skills.

For an example of of skill specialization, we may categorize \sk{shortsword}{}
and \sk{broadsword}{} as specializations of \sk{swordplay}{} and distinguish
\sk{rapier}{} from these by categorizing it as a specialization of
\sk{fencing}{}.  Thus described, the skills are called
\sk{swordplay (broadsword)}{}, \sk{swordplay (shortsword)}{}, and
\sk{fencing (rapier)}{}.  However, because specialization is a nomenclatural
convention and not a mechanic, it is an equally valid choice to group all three
skills under \sk{swordplay}{}; or to group them with all other skills relating
to hand-to-hand combat as \sk{melee (shortsword)}{}, \sk{melee (broadsword)}{},
and \sk{melee (rapier)}{}; or to group them with all other combat skills as
\sk{combat (shortsword)}{}, etc.  The groupings given in the list of skills in
this text are only suggestions.  Use whatever is most comfortable for you and
most appropriate to your campaign!

\section{Techniques}    \label{technique}
Most skills have one or more applications that are more difficult than ordinary
usage.  For example, the rather flashy martial arts move of the \sk{sweeping
kick}{} defaults \sk{skill$-3$}{} for the \sk{skill}{} representing the martial
art in which the fighter has trained.  However, if the GM allows, it may be
possible to buy off this $-3$ malus by learning the move as a \kw{technique}.

A technique is any specific application of a skill that may be studied and
perfected separately from the rest of the skill.  It may be thought of as a
sort of meta-skill: it has a default, prerequisites, and a difficulty, but is
based on a skill rather than an attribute.

It should be noted that techniques are a relatively advanced rule easily
dispensable for simplicity's sake.  \Nokitchensink.

\subsection{Base Skill} \label{techbaseskill}
When a technique is bought, a base skill must be specified.  No technique may
be used with any skill other than the base skill for which it was bought. For
example, although you may buy the \sk{sweeping kick}{} for any of the martial
arts skills of \sk{striking art}{}, \sk{grappling art}{}, and \sk{shoving
art}{}, if you buy it for \sk{striking art}{}, \sk{striking art}{} will be the
base skill for that technique;  if you wish to use \sk{grappling art}{} for
sweeping kicks at no malus, you must buy the \sk{sweeping kick}{} technique for
\sk{grappling art}{} as well.

On the character sheet, a technique's base skill is written in parentheses
after the name of technique, as in \sk{sweeping kick}{\textit{striking art}}.

\subsection{Technique Difficulty and Cost}      \label{techcost}
A technique may be \textit{Easy} or \textit{Hard}.  \textit{Easy} techniques
cost [1] per level.  \Hard\ techniques also cost [1] per level, except for the
first level, which costs [2].

\subsection{Technique Limits}   \label{techlimit}
Some techniques are \kw{limited} and cannot be raised above their default
skills, while others are \kw{unlimited} and subject to no such restriction.
Whatever action is governed by an unlimited technique may, if more levels of
the technique are bought than the technique's default malus, will, in fact, be
rolled at a \textit{bonus} relative to the rest of the skill.  However, for
limited techniques, once the default malus is bought off to zero, the technique
may not be improved further.  For other techniques, this limit it is the score
of the default skill plus a bonus or malus.

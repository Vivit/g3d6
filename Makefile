# root directory (run config to update)
root=/home/vivit/doc/tex/g3d6

aux=$(root)/aux
src=$(root)/src
pdf=$(root)/pdf
log=$(root)/log
percent := %

main=main.tex
TeX=pdflatex

all: ready text done

# hypertext: ready hyperready text hyperdone done


# Horrible, horrible kludge
# hyperready:
#	mv -f .hyperopt.pre.tex hyperopt.pre.tex

# hyperdone:
#	mv -f hyperopt.pre.tex .hyperopt.pre.tex
#	touch hyperopt.pre.tex
#	mv main.pdf main.hyper.pdf

ready:
	if [ ! -d $(root) ]; then mkdir $(root); fi
	find $(aux) -regex ".*aux$$" -type f -execdir mv '{}' -t $(root) \;
	find $(src) -regex ".*tex$$" -type f -execdir mv '{}' -t $(root) \;

text:
	$(TeX) $(root)/$(main) ; true

done:
	find $(root)/* -maxdepth 0 -regex ".*aux" -type f -execdir mv '{}' -t $(aux) \;
	find $(root)/* -maxdepth 0 -regex ".*pdf" -type f -execdir mv '{}' -t $(pdf) \;
	find $(root)/* -maxdepth 0 -regex ".*tex" -type f -execdir mv '{}' -t $(src) \;
	find $(root)/* -maxdepth 0 -regex ".*log" -type f -execdir mv '{}' -t $(log) \;
	find $(src)/* -maxdepth 0 -regex ".*\.pre\..*" -type f -execdir mv '{}' -t $(src)/preamble \;
	find $(src)/* -maxdepth 0 -regex ".*/ch[0-9]*\.tex" -type f -execdir mv  '{}' -t $(src)/chapters \; 

clean:
	rm -f $(pdf)/*


test: ready testtext done

testtext: 
	$(TeX) $(root)/test.tex ; true

spotless: clean
	rm -f $(aux)/*
